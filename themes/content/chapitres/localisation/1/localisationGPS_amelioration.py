# SNT seconde
# Thème Cartographie et localisation
#  
# Utilisation d'un fichier de coordonnées GPS
#
#(C) Stéphane Colomban 2019  (CC : by - nc - sa)

# coordonnées GPS de Paris et Marseille récupérées sur https://www.gps-longitude-latitude.net
# Fichier .csv des coordonnées des villes de France récupéré sur : https://sql.sh/736-base-donnees-villes-francaises



from tkinter import *


# Remarque, voici les le corrigé de votre tableau:
##################################################
# latitudes et ordonnées écran
# latitude de Marseille = 43.2965
# y de Marseille = 657
# latitude de Paris = 48.8566
# y de paris Paris = 255

# longitudes et abscisses écran
# longitude de Marseille = 5.3698
# x de Marseille = 585
# longitude de Paris = 2.3522
# x de Paris = 416
###################################################


afficheCoordo=True

## Calcul des coordonnées GPS à l'aide des coodonnées (x;y)
## Compléter les lignes latitude = ...... et longitude = ......
def g(y):
     
    latitude = 0  #Enlever le 0 et mettre votre formule
     
    return (int(latitude * 1000) / 1000.)

def f(x):
     
    longitude = 0  #Enlever le 0 et mettre votre formule
     
    return (int(longitude * 1000) / 1000.)



######################## INTERFACE GRAPHIQUE -NE PAS MODIFIER - ################ 


def motion(event):
    """ Evénements liés à la souris """
    x=event.x
    y=event.y
    if (afficheCoordo):
        canevas.delete(ALL)
        canevas.create_image(10,85,anchor=NW, image=img)
        canevas.create_line(0,y,760,y,fill='blue')
        canevas.create_line(x,40,x,780,fill='red')
        canevas.create_text(850,200, text="x ="+ str(x),fill='red')
        canevas.create_text(850,230, text="y ="+ str(y),fill='blue')
#        canevas.create_text(850,300, text="longitude ="+ str(f(x)),fill='red')
#        canevas.create_text(850,330, text="latitude ="+ str(g(y)),fill='blue')
     

##################################### 
# Création de l'interface graphique #
##################################### 
# Création de la fenêtre principale 
root = Tk()
root.title('Cartographie - SNT 2019')


# Création d'un widget Canvas (zone graphique contenant le fond de carte)
LARGEUR = 950
HAUTEUR = 800
canevas = Canvas(root, width = LARGEUR, height = HAUTEUR, bg ='white')
img = PhotoImage(file="fond_FRANCE.png")
canevas.create_image(5,85,anchor=NW, image=img)
canevas.pack(padx =5, pady =5)




# Mise à jour de l'interface
root.bind('<Motion>', motion)
root.mainloop()
