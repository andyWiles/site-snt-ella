from random import randint
choix=randint(0,9)
devine=int(input("J'ai choisi un chiffre (entier entre 0 et 9). \nEssais de le deviner : "))
while devine!=choix :
    if devine > choix:
        print("Trop grand.")
    else:
        print("Trop petit.")
    devine=int(input("Essais encore : "))
print("Tu as enfin trouvé !")
