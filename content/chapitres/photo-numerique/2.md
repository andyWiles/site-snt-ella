---
title: "Photo numérique - séance 2"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "photo-numerique"
---
![photo-numerique](../photonumerique.png#small-align-right)


## manipulation d'images avec GIMP



### But de la séance    

- Comprendre le fonctionnement des calques
- Savoir importer différentes images sur différents calques avec GIMP
- Savoir déplacer un calque
- Savoir détourer le sujet d'un calque
- Savoir exporter une création avec GIMP


{{% vignettepdf "télécharger la fiche PHOTONUMERIQUE1" %}}../PHOTO_S2__OK.pdf{{% /vignettepdf %}}


### Documents nécessaires

#### Partie 1



1. Image d'arrière plan [gimp-vienne-arriere-plan.jpg](gimp-vienne-arriere-plan.jpg)
2. Image du chien de premier plan [gimp-chien.jpg](gimp-chien.jpg)
3. Image d'un deuxième chien de premier plan [gimp-chien2.jpg](gimp-chien2.jpg)
4. Image d'un troisième chien de premier plan [gimp-chien3.jpg](gimp-chien3.jpg)


#### Partie 2



1. Image d'arrière plan [piece.png](piece.png)
2. Premier tableau [joconde.png](joconde.png)
3. Deuxième tableau [nympheas.png](nympheas.png)
4. Troisième tableau [jeune-fille-a-la-perle.png](jeune-fille-a-la-perle.png)


![gimp-vienne-final.jpg](gimp-vienne-final.jpg#small-align-right)
![gimp-piece-et-tableaux.png](gimp-piece-et-tableaux.png#small-align-right)
