---
title: " Structures élémentaires"
date: 2019-08-04
background : "python.png"
---

![Python](../python.png#small-align-right)

#### 1. Ecrire du texte ou la valeur d'une variable ####
``` Python
print("bonjour")
a=15
print(a)
texte="toto"
print(texte)
```

#### 2. Ecrire un test conditionnel  
###### Ecrivons un programme qui demande notre âge et affiche, suivant le cas, si on est majeur ou mineur  

``` Python
age = eval (input("Saisissez votre âge :"))
if age<18:
    print("Vous êtes mineur·e")
else:
    print("Vous êtes majeur·e")
```
#### 3. Ecrire une boucle conditionnelle (ou boucle non bornée)  ####

###### Vous disposez de 10€ et économisez 3€ par jour. 
######  Au bout de quelle durée pourrez-vous acheter une tablette à 100€ ?  

``` Python
duree = 0
somme  = 10
while somme <100 :
    duree = duree + 1
    somme = somme + 3
    
print("Durée attendue :",duree)
```

#### 4. Ecrire une boucle itérative (ou boucle  bornée)  ####

###### Vous disposez de 10€ et économisez 3€ par jour pendant 31 jours.
######  De quelle somme disposerez-vous ?  

``` Python
somme  = 10
for jour in range(1,31+1):
    somme = somme + 3

print("Somme économisée:",somme)
```

