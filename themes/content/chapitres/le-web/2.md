---
title: "Le Web - séance 2"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "le-web"
---
![Le-Web](../leweb.png#small-align-right)


## - WEB séance 2 - Introduction au CSS

 
### But de la séance 

- Comprendre le principe de la décoration à l'aide d'une feuille de style css.
- Apprendre à lier une décoration css à une page html.
- S'approprier les principales propriétés css.




<center>{{% docpdf "télécharger la fiche HTML2" %}}../WEB_S2__v3.pdf{{% /docpdf %}}</center>




### Aide mémoire :

##### Structure d'une page html avec lien vers le fichier css
```HTML
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> montitre </title>
        <link href="madecoration.css" rel="stylesheet" />
    </head>
    <body>
        ... tout le corps de la page ...
    </body>
</html>
```


##### Principales propriétés css
```CSS
.mabalise {
    text-align : center ;           alignement du texte : left ou center ou right
    font-size : 20px ;              taille en pixels de la police de caractères
    font-weight : 700 ;             graisse de la police de caractères (100=léger ←→ 900=gras)
    font-style : italic ;           style de la police : italic ou normal ou oblique 
    background : #FF0000 ;          couleur d’arrière plan (ici rouge)
    color : #0000FF ;               couleur de la police de caractère (ici bleue)
    border : solid 3px #0000FF ;    bordure solide, bleue de 3px : solid ou dotted ou dashed
}
```


{{% notice note %}} 
Faire une fiche cours afin de mémoriser ces balises.
{{% /notice %}}





 
