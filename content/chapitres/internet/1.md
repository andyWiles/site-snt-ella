---
title: "L'internet - séance 1"
date: 2019-06-11T17:26:13+02:00
masque: "oui"
chapitre: "internet"
---
![internet](../internet.png#small-align-right)


## Séance 1 - adresses symboliques

### But de la séance

- Comprendre le principe d'une adresse symboliques
- Comprendre le principe d'un serveur DNS
- Créer un mini réseau local avec Filius
- Réviser l'optimisation du parcours sur un graphe avec Dijkstra


{{% vignettepdf "télécharger la fiche INTERNET1" %}}../INTERNET_S1__OK.pdf{{% /vignettepdf %}}  
