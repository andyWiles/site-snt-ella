---
title: "Le web"
date: 2019-06-11T17:26:13+02:00
background : "le-web/leweb.png"
---
![Le-Web](leweb.png#small-align-right)





| Lien vers la séance  | Description  |
|:--------:|:------------:|
| [séance 1](1)   | INTRODUCTION AU HTML         |   
| [séance 2](2)   | INTRODUCTION AU CSS          |                
| [séance 3](3)   | POSITIONNER DES BLOCS        |
| [séance 4](4)   | DS : DÉCORER DES BLOCS        |
| [séance 5](5)   | PROJET : FABRIQUER UNE FAKE NEWS       | 
﻿
