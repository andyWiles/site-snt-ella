---
title: "Langage Python"
date: 2024-09-02
background : "python/python.png"
---
![python](python.png#small-align-right)


| Lien vers la fiche  | Description  |
|:--------:|:------------:|
| [Fiche 1](PYTHON_S1.pdf)   | Découverte de Python  |   
| [Fiche 1bis](PYTHON_S1_module.pdf)   | Fiche d'exercices en Python  |   
| [Fiche 2](PYTHON_S2.pdf)   | Tests et Boucles  (TP noté)         |                
| [Fiche 3](PYTHON_S3.pdf)   | Perfectionnement et mini projet      |        |
| [Fiche 4](PYTHON_S4.pdf)   | Devoir surveillé    |
| [Fiche 5](PYTHON_S5.pdf)   | Les procédures en Python    |
| [Fiche 6](PYTHON_MOTS_CLES.pdf)    | Liste finale des mots clés  |
| [PROJET](PYTHON_S6_JEU.pdf)    | Créer un jeu en python  |


![Python](python.png#small-align-right)
