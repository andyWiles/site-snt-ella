---
title: "Les objets connectés"
date: 2019-29-11T17:26:13+02:00
background : "objets-connectes/IOT.png"
---
![IOT](IOT.png#small-align-right)




| Lien vers la séance   | Description  |
|:---------:|:------------:|
| [séance 1](1)          | PRÉSENTATION ET AFFICHAGES LED SUR CARTE MICRO:BIT
| [séance 2](2)          | BOUTONS SUR CARTE MICRO:BIT
| [séance 3](3)          | CAPTEURS SUR CARTE MICRO:BIT






 
