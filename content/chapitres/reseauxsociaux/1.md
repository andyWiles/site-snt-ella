---
title: "Réseaux sociaux - séance 1"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "reseauxsociaux"
---
![reseauxsociaux](../reseauxsociaux.png#small-align-right)


##  LES MÉCANISMES DES RÉSEAUX SOCIAUX



### But de la séance

- Comprendre les mécanismes des réseaux sociaux à travers l'étude d'articles de presse



{{% vignettepdf "télécharger la fiche RÉSEAUX SOCIAUX 1" %}}../RS_01__OK.pdf{{% /vignettepdf %}}
