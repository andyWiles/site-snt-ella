---
title: "Les procédures"
date: 2019-08-03
background : "python.png"
---

![Python](../python.png#small-align-right)

#### 1. Comment définir une procédure ? ####
``` Python
def nom_fonction(arg1, arg2, arg3, ...):
    ...instructions...
    
```

#### 2. Exemple 1
###### Ecrivons une procédure ayant pour arguments deux entiers a et b et qui affiche leur somme  

``` Python
def affichesomme(a,b):
    s=a+b
    print("somme =",s)
```
#### 3. Exemple 2
###### Ecrivons une procédure ayant pour arguments deux entiers a et b et qui affiche le plus grand des deux
 
``` Python
def afficheplusgrand(a,b):
    if a<b:
        p=b
    else:
        p=a
    print("le plus grand est ",p)
```

#### 4. Exemple 3
###### Ecrivons une procédure ayant pour arguments trois entiers a,b et c  et qui affiche leur moyenne
 
``` Python
def affichemoyenne(a,b,c):
    moy = (a+b+c)/3
    print("moyenne =",moy)
```

#### 5. Exemple 4
###### Ecrivons une procédure ayant pour argument  un entier positif n  et qui affiche la somme 1+2+3+...+n
 
``` Python
def somme(n):
   s = 0
   for k in range(1,n+1):
       s = s + k 
   
   print("somme des entiers de 1 à n :",s)
```