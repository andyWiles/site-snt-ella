---
title: "Photo numérique - séance 1"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "photo-numerique"
---
![photo-numerique](../photonumerique.png#small-align-right)


## principe de fonctionnement d'un capteur CCD



### But de la séance

- Comprendre le principe d'un capteur CCD
- Connaître le vocabulaire associé (pixel, photosite)
- Maîtriser le codage RVB d'une couleur
- Connaître le codage RVB de quelques couleurs de référence
- Ne pas confondre la résolution et la définition d'une photo numérique
- Savoir éditer et modifier un programme python.


{{% vignettepdf "télécharger la fiche PHOTONUMERIQUE1" %}}../PHOTO_S1__OK.pdf{{% /vignettepdf %}}

### Documents nécessaires

1. Script Python [S1_photonum.py](S1_photonum.py)
3. Image [S1_joconde.png](S1_joconde.png)
