---
title: "Les fonctions"
date: 2019-08-02
background : "python.png"
---

![Python](../python.png#small-align-right)

#### Une fonction a la même structure qu'une procédure mais  elle se termine par l'instruction ``return`` afin de retourner une valeur.


#### 1. Comment définir une fonction ? ####
``` Python
def nom_fonction(arg1, arg2, arg3, ...):
    ...instructions...
    return(valeur)
```

#### 2. Exemple 1
###### Ecrivons une fonction ayant pour arguments deux entiers a et b et qui retourne la somme de leurs carrés  

``` Python
def sommecarre(a,b):
    s=a**2+b**2
    return(s)
```

###### Appelons deux fois cette fonction dans le programme principal :
``` Python
 s=sommecarre(10,20) + sommecarre(8,14)
 print(s)
```


#### 3. Exemple 2####
###### Ecrivons une fonction ayant pour arguments deux entiers a et b et qui retourne le plus grand des deux
 
``` Python
def plusgrand(a,b):
    if a<b:
        p=b
    else:
        p=a
    return(p)
```

#### 4. Exemple 3####
###### Ecrivons une fonction ayant pour arguments trois entiers a,b et c  et qui retourne leur moyenne
 
``` Python
def moyenne(a,b,c):
    moy = (a+b+c)/3
    return(moy)
```

#### 5. Exemple 4####
###### Ecrivons une fonction ayant pour argument  un entier positif n  et qui retourne la somme 1+2+3+...+n
 
``` Python
def somme(n):
   s = 0
   for k in range(1,n+1):
       s = s + k 
   
   return(s)
```