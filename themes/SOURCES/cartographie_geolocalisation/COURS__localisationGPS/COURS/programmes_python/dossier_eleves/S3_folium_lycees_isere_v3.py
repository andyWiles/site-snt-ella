# SNT seconde
# Thème Cartographie et localisation
#  
# Lycées de l'Isère
# Utilisation d'un fichier .CSV de coordonnées GPS
#
#(C) Stéphane Colomban 2019  (CC : by - nc - sa)


# bibliothèques nécessaires
import folium
import pandas    #pour importer et analyser un fichier  .csv

   

#Création d'une carte interactive de l'Isère
carte= folium.Map(location=[45.188529, 5.724524],zoom_start=9)

#On superpose les contours du département de l'Isère stockés dans le fichier "departement-38-isere.geojson"
file = 'departement-38-isere.geojson'
folium.GeoJson(file,name='Isère').add_to(carte)

#Ajout des lycées sur la carte
fichier = pandas.read_csv('S3_etablissements.csv',delimiter=";", usecols = ["Appellation officielle","Nature","Code postal","Latitude", "Longitude"])
for n in range(len(fichier["Code postal"])):
    codepostal=float(fichier["Code postal"][n])
    nature=fichier["Nature"][n]
    if (int(codepostal/1000)==38 or codepostal==69560) and nature[0:5]=="LYCEE": #On est en Isère et on a affaire à un lycée
        nom =fichier["Appellation officielle"][n]
        lat =float(fichier["Latitude"][n])
        lon =float(fichier["Longitude"][n])
        print(nom)
        folium.Marker(location=[lat, lon], popup=nom).add_to(carte)
        
#sauvegarde de la carte
carte.save('carteLYCEES.html')
