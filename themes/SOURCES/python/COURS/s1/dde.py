#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#       _                  _              _     _
#      | |                | |            (_)   | |
#      | | ___ _   _    __| | ___   _ __  _ ___| |_ ___
#  _   | |/ _ \ | | |  / _` |/ _ \ | '_ \| / __| __/ _ \
# | |__| |  __/ |_| | | (_| |  __/ | |_) | \__ \ ||  __/
#  \____/ \___|\__,_|  \__,_|\___| | .__/|_|___/\__\___|
#                                  | |
#                                  |_|
#
# Il ne faut pas afficher ce fichier, mais l'exécuter.
#
################################################################################

import base64
import os
import subprocess
import tempfile
import webbrowser

TEMPLATE = """
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>{title}</title>
  </head>
  <body>
    <p>{content}</p>

    <hr>

    <p><a href="http://lpaternault.frama.io/jeu-de-piste-linux/03-9dc2ce470165e32478cf71a1e012ba49/">Recommencer.</a></p>
  </body>
</html>
"""

BRAVO = b"PHA+TGEgc3VpdGUgw6AgbCdhZHJlc3NlIDxhIGhyZWY9aHR0cDovL2xwYXRlcm5hdWx0LmZyYW1hLmlvL2pldS1kZS1waXN0ZS1saW51eC8wNC0yN2NkMjE5ZjBkMzI5ODBkMDZiOTk3YTcwYWRjNGM4Nj5odHRwOi8vbHBhdGVybmF1bHQuZnJhbWEuaW8vamV1LWRlLXBpc3RlLWxpbnV4LzA0LTI3Y2QyMTlmMGQzMjk4MGQwNmI5OTdhNzBhZGM0Yzg2PC9hPi48L3A+Cg=="

ERREUR = b"Tm9uICEgTGUgZmljaGllciBuJ2VzdCBwYXMgYXUgYm9uIGVuZHJvaXQgOiBpbCBlc3QgZGFucyBsZSByw6lwZXJ0b2lyZSDCqyB7YWN0dWVsfSDCuywgaWwgZGV2cmFpdCDDqnRyZSBkYW5zIMKrIHtjaWJsZX0gwrsuCg=="

def bureau():
    """Récupération du chemin vers le bureau"""
    try:
        return subprocess.check_output(['xdg-user-dir', 'DESKTOP']).decode().strip()
    except:
        for candidat in ("Desktop", "Bureau"):
            absolute = os.path.expanduser(os.path.join("~", candidat))
            if os.path.isdir(absolute):
                return absolute
    return os.path.expanduser("~/Bureau")

def decode(text):
    return base64.b64decode(text).decode().strip()

if __name__ == "__main__":
    cible = os.path.abspath(os.path.join(bureau(), "TP0"))
    repertoire = os.path.abspath(os.path.dirname(__file__))
    if cible.lower() == repertoire.lower():
        message = decode(BRAVO)
    else:
        message = decode(ERREUR).format(actuel=repertoire, cible=cible)
    (filedescriptor, filename) = tempfile.mkstemp(text=True)
    with os.fdopen(filedescriptor, mode="wt") as file:
        file.write(TEMPLATE.format(
        title="Étape 3",
        content=message,
        ))
    webbrowser.open("file://{}".format(filename))
