---
title: "Fiches python"
date: 2019-08-01
background : "python.png"
---





## Fiches cours 

| Lien vers la fiche  | Description  |
|:--------:|:------------:|
| [Fiche 1](../PYTHON_S1__v2.pdf)   | Instructions élémentaires         |   
| [Fiche 2](../PYTHON_S2__v1.pdf)   | La boucle "tant que"          |                
| [Fiche 3](../PYTHON_liste_des_mots_cles.pdf)   | Résumé provisoire des mots clés       | 
| [Evaluation de mi-parcours](../PYTHON_interro_de_cours.pdf)   | Evaluation de mi-parcours : interrogation de cours      | 
| [Corrigé](../PYTHON_interro_de_cours_corrige.pdf)   | Corrigé de l'interrogation de cours      | 
| [Fiche 4](../PYTHON_S4__v1.pdf)   | La boucle "pour"          | 
| [Fiche 5](../PYTHON_S5__v1.pdf)   | TP Noté : Notion de procédure          |
| [Fiche bilan](../PYTHON_liste_des_mots_cles_version_finale.pdf)   | Liste finale des mots clés  |
| [Evaluation finale](../PYTHON_EVAL.pdf)   | Evaluation finale  |

![Python](../python.png#small-align-right)

