---
title: "Le Web - séance 7"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "le-web"
---

![Le-Web](../leweb.png#small-align-right)


## - WEB séance 7 - Notion de requête HTTP

### But de la séance 

- Comprendre la notion de client et de serveur
- Savoir coder une requête HTTP



<center>{{% docpdf "télécharger la fiche HTML7" %}}../WEB_S7__v1.pdf{{% /docpdf %}}</center>



### Aide mémoire :

##### Balisage d'un formulaire de requête par exemple sur wikipedia
```HTML
<form action="https://fr.wikipedia.org/wiki" >
  <input type="input" name="search" />
  <input type="submit" /> 
</form>
```




{{% notice note %}} 
Faire une fiche cours afin de mémoriser ces balises.
{{% /notice %}}




 
