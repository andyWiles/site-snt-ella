---
title: "Le Web - séance 3"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "le-web"
---
![Le-Web](../leweb.png#small-align-right)


## - WEB séance 3 - Positionner des blocs

### But de la séance 

- Se perfectionner avec la décoration css.
- Etre capable de rajouter des marges à un bloc css
- S'approprier les principales propriétés css.



<center>{{% docpdf "télécharger la fiche HTML3" %}}../WEB_S3__v3.pdf{{% /docpdf %}} </center>




### Aide mémoire :

##### Balisage d'un bloc
```HTML
<div class="monbloc">
    ... contenu du bloc ...
</div>
```


##### Principales propriétés css
```CSS
.monbloc {
    background : #FF0000 ;          couleur d’arrière plan (ici rouge)
    color : #0000FF ;               couleur de la police de caractère (ici bleue)
    
    margin : 10px 20px 30px 10px ;  marges extérieures dans l’ordre h-g-b-d
    border : solid 3px #0000FF ;    bordure solide, bleue de 3px : solid ou dotted ou dashed
    padding : 10px 20px 30px 10px ; marges intérieures dans l’ordre h-g-b-d
    
    text-align : center ;           alignement du texte : left ou center ou right
    font-size : 20px ;              taille en pixels de la police de caractères
    font-weight : 700 ;             graisse de la police de caractères (100=léger ←→ 900=gras)
    font-style : italic ;           style de la police : italic ou normal ou oblique
}
```

{{% notice note %}} 
Faire une fiche cours afin de mémoriser ces balises.
{{% /notice %}}


 
