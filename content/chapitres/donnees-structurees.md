---
title: "Les données structurées"
date: 2019-29-11T17:26:13+02:00
background : "donnees-structurees/donnees-structurees.png"
---
![données structurées](donnees-structurees.png#small-align-right)




| Lien vers la séance  | Description  |
|:--------:|:------------:|
| [séance 1](1)   |  ETUDE DE DONNÉES PUBLIQUES       |   
| [séance 2](2)   |  ETUDE D'UN FICHIER CSV       |  
| [séance 3](3)   |  DEVOIR SURVEILLÉ      | 
