# SNT seconde
# Thème Cartographie et localisation
#  
# Utilisation d'un fichier .CSV de coordonnées GPS
#
#(C) Stéphane Colomban 2019  (CC : by - nc - sa)


# bibliothèques nécessaires
import csv


#Création de l'objet Lycée
class Lycee:
    def __init__(self,nom,lat,lon):
        self.nom = nom
        self.lat = lat
        self.lon = lon
        
    
    
# Remplissage de la liste des lycée à partir du fichier .csv
lycees=[]
with open("export_etab_test.csv","r") as csvfile:
    fich = csv.reader(csvfile, delimiter=',', quotechar='"')
    
    for row in fich:
        lycees.append( Lycee(row[4], eval(row[15].replace(',','.')) , eval(row[16].replace(',','.')))  )
          
             

# Affichage du nom et coordonnées des lycées de l'Isère
for n in range(len(lycees)):
   print(lycees[n].nom,lycees[n].lat,lycees[n].lon)
   
   
   

#Création d'une carte interactive de l'Isère
import folium

