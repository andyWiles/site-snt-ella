---
title: "Objets connectés - séance 1"
date: 2019-12-07
masque : "oui"
chapitre : "objets-connectes"
---
![objets-connectes](../IOT.png#small-align-right)


## - Objets Connectés séance 1 - Affichages LED sur micro:bit

### But de la séance 

- Comprendre le fonctionnement d'une carte micro:bit
- S'approprier les principales instructions python




<center>{{% docpdf "télécharger la fiche MICROBIT1" %}}../MICROBIT_S1.pdf{{% /docpdf %}} </center>
 




### Aide mémoire 

##### Images intégrées à Micro:bit

    Image.HEART
    Image.HEART_SMALL
    Image.HAPPY
    Image.SMILE
    Image.SAD
    Image.CONFUSED
    Image.ANGRY
    Image.ASLEEP
    Image.SURPRISED
    Image.SILLY
    Image.FABULOUS
    Image.MEH
    Image.YES
    Image.NO
    Image.CLOCK12, Image.CLOCK11, ... , Image.CLOCK1
    Image.ARROW_N, Image.ARROW_NE, Image.ARROW_E, Image.ARROW_SE
    Image.ARROW_S, Image.ARROW_SW, Image.ARROW_W, Image.ARROW_NW
    Image.TRIANGLE
    Image.TRIANGLE_LEFT
    Image.CHESSBOARD
    Image.DIAMOND
    Image.DIAMOND_SMALL
    Image.SQUARE
    Image.SQUARE_SMALL
    Image.RABBIT
    Image.COW
    Image.MUSIC_CROTCHET
    Image.MUSIC_QUAVER
    Image.MUSIC_QUAVERS
    Image.PITCHFORK
    Image.XMAS
    Image.PACMAN
    Image.TARGET
    Image.TSHIRT
    Image.ROLLERSKATE
    Image.DUCK
    Image.HOUSE
    Image.TORTOISE
    Image.BUTTERFLY
    Image.STICKFIGURE
    Image.GHOST
    Image.SWORD
    Image.GIRAFFE
    Image.SKULL
    Image.UMBRELLA
    Image.SNAKE


