---
title: "Les objets connectés"
date: 2021-03-11T17:26:13+02:00
background : "objets-connectes/IOT.png"
---
![IOT](IOT.png#small-align-right)




| Lien vers la séance   | Description  |
|:---------:|:------------:|
| [séance 1](1)          | PRÉSENTATION ET AFFICHAGES LED SUR CARTE MICRO:BIT
| [séance 2](2)          | BOUTONS et CAPTEURS SUR CARTE MICRO:BIT
| [séance 3](3)          | TP NOTÉ SUR CARTE MICRO:BIT
