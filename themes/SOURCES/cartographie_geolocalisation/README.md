## Cartographie ##


### Cette partie contient :
 - dans **localisationGPS :**   
    - une **base de programme**  en python permettant le lire un .csv contenant des coordonnées GPS  (facile à compléter pour faire afficher des communes suivant des besoins ou créer des quiz)
    - une **base de cours** de la première séance (à compléter) 

Pour faciliter la création des images .eps penser à installer le paquet  **imageMagick**
```
sudo apt install imageMagick (sous Linux)
sudo port install imageMagick (sous mac)
convert monImage.png monImageconvertie.eps
```

 - dans **ArduinoGPSmaster :** 
 
    - un exemple de projet de boitier GPS à base d'arduino
    
 
 - dans **geospatial-analysis :** un super projet  qui manipule des cartes interactives 
 
    - Sources sur  https://github.com/makinacorpus/tutorials/tree/master/geospatial-analysis
 
    - Démo sur https://makina-corpus.com/blog/metier/2019/python-carto
 
    - Nécessite d'installer jupyter notebook et ipython :


 
```
python3 -m pip install jupyter
python3 -m pip install ipython
jupyter notebook
```
  
  ### Liens intéressants :
- https://leafletjs.com

    - script javascript permettant de mettre des cartes interactives dans un fichier HTML
  
- https://france-geojson.gregoiredavid.fr

    - bases de données sur les communes, départements et régions de France
  
- https://sql.sh/736-base-donnees-villes-francaises

    - bases de données sur les communes de France
