---
title: "Commandes principales"
date: 2019-08-03
background : "python.png"

---

![Python](../python.png#small-align-right)



### Variables et Affectations  

``` Python
variable = valeur                    # affectation simple
var1, var2, var3 = val1, val2, val3  # affectation multiple
del variable                         # destruction de variable
```


### Opérations logiques  
``` Python
b1 or b2    # b1 ou b2
b1 and b2   # b1 et b2
not b1      # contraire de b1
```

### Comparaison entre deux valeurs  
``` Python
x < y    # x inférieur à  y 
x > y    # x supérieur  à  y 
x == y   # x égal  à  y 
x != y   # x différent de y
x <= y   # x inférieur ou égal  à  y
x >= y   # x supérieur ou égal  à  y
```

### Opérations 
``` Python
abs(x)          # Valeur absolue de x
round(x)        # Arrondi le plus proche 
int(x)          # Partie entière de x
x+y, x-y, x*y   # Somme, différence et produit
x/y             # Division réelle de x par y
x//y            # Partie entière du quotient de x par y  (division euclidienne si a et b  entiers)
x%y             # Reste de la division  de x par y
x**y            # x puissance y
```


### Entrées et sorties
``` Python
print(var)                          # affiche la variable var à l'écran
print(var1, var2, var3)             # affiche les variables à l'écran
print("bonjour")                    # affiche bonjour à l'écran

mot=input("Quel est votre mot ?")      # saisie au clavier de la chaine mot
n=int(input("Quel est votre entier ?"))   # saisie au clavier de l'entier n
x=float(input("Quel est votre décimal ?")) # saisie au clavier du décimal x
y=eval(input("Quelle est votre valeur numérique ?"))  # saisie au clavier de la valeur numérique y
```



### Fonctions et procédures
``` Python
def ma_procedure(arg1, arg2,arg3,...):   # définition de la procédure appelée maprocedure
     ...instructions...
    
def ma_fonction(arg1, arg2,arg3,...):    # définition de la fonction appelée mafonction
    ...instructions...   
    return (valeur)                     # valeur retournée
```



### Tests et boucles

#### > test conditionnel
``` Python
if (condition):
     ...instructions 1... 
else :
     ...instructions 2...
```


#### > boucle conditionnelle (non bornée)
``` Python
while (condition):
     ...instructions... 

```

#### > boucle itérative (bornée)
``` Python
for variable in range(a,b+1):    # pour variable dans [a;b+1[ 
     ...instructions...          # donc prenant les valeurs a, a+1, a+2, ...., b
```

