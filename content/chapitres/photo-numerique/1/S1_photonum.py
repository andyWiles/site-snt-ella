# SNT seconde
# Thème : Photographie
#
# Création d'une interface graphique pour manipuler des images
#
# (C) S. Colomban 2021  (CC : by - nc - sa)



from tkinter import *
from PIL import Image, ImageTk

def charger_image(filename, resize=None):
    img = Image.open(filename)
 
    if resize is not None:
        img = img.resize(resize, Image.ANTIALIAS)
 
    return img
 
 

def composanterouge(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
    for x in range(largeur):
        for y in range(hauteur):
            (rouge,vert,bleu) = img.getpixel((x,y))
            vert = 0
            bleu = 0
            
            image.putpixel((x,y),(rouge,vert,bleu))
             
 
    return image


def composanteverte(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
      # à compléter       
      # à compléter 
    return image

def composantebleue(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
        # à compléter      
        # à compléter 
    return image



img0 = charger_image('S1_joconde.png', resize=(200,200) )
img1 = composanterouge(img0)
img2 = composanteverte(img0)
img3 = composantebleue(img0)




###############################################
#### Interface graphique (ne pas modifier) ####


def motion(event):
    """ Evénements liés à la souris """
    x=event.x
    y=event.y
    
     
def actionBouton1():
    """ Actions du bouton 1"""
    canevas.create_image(50,300,anchor=NW, image=photo1)
    #canevas.create_text(850,200, text="BOUTON1 PRESSE",fill='blue')
    canevas.pack(padx =5, pady =5)

def actionBouton2():
    """ Actions du bouton 2"""
    canevas.create_image(350,300,anchor=NW, image=photo2)
    canevas.pack(padx =5, pady =5)

def actionBouton3():
    """ Actions du bouton 3"""
    canevas.create_image(650,300,anchor=NW, image=photo3)
    canevas.pack(padx =5, pady =5)

    
# Création de la fenêtre principale 
root = Tk()
root.title('Photo numérique - Lycée Ella Fitzgerald - SNT 2019')



# Création d'un widget Canvas 


LARGEUR = 900
HAUTEUR = 550
canevas = Canvas(root, width = LARGEUR, height = HAUTEUR, bg ='white')

photo0=ImageTk.PhotoImage(img0)
photo1=ImageTk.PhotoImage(img1)
photo2=ImageTk.PhotoImage(img2)
photo3=ImageTk.PhotoImage(img3)


canevas.create_image(350,50,anchor=NW, image=photo0)
canevas.pack(padx =5, pady =5)


cadre_a_boutons = Frame(root, height=200, width=250)
cadre_a_boutons.pack_propagate(0)
cadre_a_boutons.pack()

# Création d'un widget Button (bouton 1)
bouton1 = Button(cadre_a_boutons, text ='composante rouge', command = actionBouton1)
bouton1.pack(fill=BOTH,expand=1,pady=5)

# Création d'un widget Button (bouton 2)
bouton2 = Button(cadre_a_boutons, text ='composante verte (à compléter)', command = actionBouton2)
bouton2.pack(fill=BOTH,expand=1,pady=5)

# Création d'un widget Button (bouton 3)
bouton3 = Button(cadre_a_boutons, text ='composante bleue (à compléter)', command = actionBouton3)
bouton3.pack(fill=BOTH,expand=1,pady=5)


# Mise à jour de l'interface
root.bind('<Motion>', motion)
root.mainloop()
