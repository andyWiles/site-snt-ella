# Geospatial analysis

Ce projet utilise Jupyter notebooks pour explorer des données géographiques

## Démonstration
https://nbviewer.jupyter.org/github/makinacorpus/tutorials/blob/master/geospatial-analysis/notebooks/Snow_map.ipynb



## Sources

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Sources sur  https://github.com/makinacorpus/tutorials/tree/master/geospatial-analysis
 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-  Démo sur https://makina-corpus.com/blog/metier/2019/python-carto
 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Nécessite d'installer jupyter notebook et ipython
 
        python3 -m pip install jupyter 
     
        python3 -m pip install ipython
 
 Sous Windows, apparamment il suffit d'installer ANACONDA
 
https://www.anaconda.com/distribution/

  
  
 ## Exécution
   Lancer
    ```
    jupyter notebook
    ``` 
    pour démarrer

## Références


```
git clone git://github.com/makinacorpus/tutorials/
```
