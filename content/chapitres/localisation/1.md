---
title: "Géolocalisation - séance 1"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "localisation"
---
![localisation](../localisation.png#small-align-right)


## Introduction aux coordonnées GPS



### But de la séance

- Maîtriser le vocabulaire des coordonnées géographiques
- Comprendre le fonctionnement du gps
- Savoir recherche en ligne des informations de géolocalisation d'un lieu.
- Savoir éditer et modifier un programme python.


{{% vignettepdf "télécharger la fiche LOCALISATION1" %}}../CARTO_GEO_S1__OK.pdf{{% /vignettepdf %}}


### Documents nécessaires

1. Script Python [localisationGPS.py](localisationGPS.py)
2. Script Python [localisationGPS_amelioration.py](localisationGPS_amelioration.py)
3. Image [fond_FRANCE.png](fond_FRANCE.png)
