---
title: "La photo numérique"
date: 2019-29-11T17:26:13+02:00
background : "photo-numerique/photonumerique.png"
---
![photonumerique](photonumerique.png#small-align-right)





| Lien vers la séance   | Description  |
|:---------:|:------------:|
| [1](1)          | FONCTIONNEMENT D'UN CAPTEUR CCD          
| [2](2)          | MANIPULER DES IMAGES AVEC GIMP 
| [3](3)          | MANIPULER LES PIXELS D'UNE PHOTO NUMERIQUE
 
