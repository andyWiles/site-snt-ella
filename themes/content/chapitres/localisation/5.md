---
title: "La localisation - séance 5"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "localisation"
---

## Analyse des données Exif d'une photo
![S5_photo1.jpg](S5_photo1.jpg#small-align-right)
![S5_photo2.jpg](S5_photo2.jpg#small-align-right)
![S5_photo3.jpg](S5_photo3.jpg#small-align-right)
### But de la séance 

- Savoir accéder aux données EXIF d'une photographie numérique (avec par exemple GIMP)
- Savoir interpréter ces données EXIF


<center>{{% docpdf "télécharger la fiche LOCALISATION5" %}}../CARTO_GEO_S5__v7.pdf{{% /docpdf %}} </center>



### Documents nécessaires

1. Photo numérique [S5_photo1.jpg](S5_photo1.jpg)
2. Photo numérique [S5_photo2.jpg](S5_photo2.jpg)
3. Photo numérique [S5_photo3.jpg](S5_photo3.jpg)


