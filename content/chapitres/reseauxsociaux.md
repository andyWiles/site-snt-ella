---
title: "Les réseaux sociaux"
date: 2019-29-11T17:26:13+02:00
background : "reseauxsociaux/reseauxsociaux.png"
---
![reseauxsociaux](reseauxsociaux.png#small-align-right)



| Lien vers la séance   | Description  |
|:---------:|:------------:|
| [séance 1](1)          | LES MÉCANISMES DES RÉSEAUX SOCIAUX         
| [séance 2](2)          | ANALYSE DU FILM SOCIAL DILEMMA
| [séance 3](3)          | BULLE DE FILTRAGE
