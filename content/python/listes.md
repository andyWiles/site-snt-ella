---
title: "Les listes"
date: 2019-01-01
background : "python.png"
---

![Python](../python.png#small-align-right)

#### Une liste est une suite ordonnée de valeurs de différents types.


#### 1. Comment préremplir une liste ?  
``` Python
liste = [4,5,"bonjour",-58,714,35.3]
```

#### 2. Comment accéder aux éléments d'une liste ?
``` Python
liste[0]
résultat:    4

liste[1]
résultat:    5

liste[2]
résultat:    'bonjour'

liste[-1]
résultat:    35.3

liste[-2]   
résultat:    714
```

#### 3. Comment rajouter un élément à une suite ?  

###### Méthode 1 : on utilise la méthode append() qui rajoute un élément à la fin d'une liste
``` Python
liste.append(555)
résultat:    [4,5,"bonjour",-58,714,35.3,555]
```

###### Méthode 2 : on concatène une liste contenant les éléments à rajouter
``` Python
liste = liste + [888,777]
résultat:    [4,5,"bonjour",-58,714,35.3,555,888,777]
```



#### 4. Astuce pour générer une liste rapidement :
###### Exemple 1
 ``` Python
liste = [k for k in range(0,13)]
résultat:    [0,1,2,3,4,5,6,7,8,9,10,11,12]
```
###### Exemple 2 : simulation de la somme du lancer de deux dés.
 ``` Python
from random import randint
liste = [randint(1,6)+randint(1,6) for x in range(10)]
résultat:    [7, 7, 8, 5, 6, 9, 7, 10, 2, 10]
```
