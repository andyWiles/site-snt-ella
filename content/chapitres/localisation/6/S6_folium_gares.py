# SNT seconde
# Thème Cartographie et localisation
#  
# GARES IMPORTANTES DE FRANCE
#
#
# Utilisation d'un fichier .CSV de coordonnées GPS
#
#(C) Stéphane Colomban 2019  (CC : by - nc - sa)


# bibliothèques nécessaires
import folium
import pandas    #pour importer et analyser un fichier  .csv

   

#Création d'une carte interactive de l'Isère
carte = folium.Map(location=[45.5, 5.5],zoom_start=8.5)

#On superpose les contours du département de l'Isère stockés dans le fichier "departement-38-isere.geojson"
ficISERE = "departement-38-isere.geojson"
folium.GeoJson(ficISERE,name='Isère',style_function=lambda x:{'fillColor': '#FF0000', 'color': '#FF0000'}).add_to(carte)




#Ajout des lycées sur la carte
fichier = pandas.read_csv('liste-des-gares.csv',delimiter=";", usecols = ["LIBELLE_GARE","DEPARTEMENT","X (WGS84)", "Y (WGS84)"])
c=0
for n in range(len(fichier["DEPARTEMENT"])):
    departement=fichier["DEPARTEMENT"][n]
    if departement=="Isère":
        c +=1
        lat=float(fichier["Y (WGS84)"][n])
        lon=float(fichier["X (WGS84)"][n])
        nom =fichier["LIBELLE_GARE"][n]
        print(nom)
        folium.Marker(location=[lat, lon], popup=nom).add_to(carte)
        
#sauvegarde de la carte
print("---- Nombre de gares =",c)
carte.save('carteGARES.html')
