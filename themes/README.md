# Site SNT-ELLA #
### Auteur S. COLOMBAN. 
### Professeur au lycée Ella Fitzgerald (Vienne - St-Romain-en-Gal)
#### CC  BY NC SA
Lien vers le site :
 - https://snt.entraide-ella.fr 

Site créé avec `HUGO` et son thème `ghostwriter`
- Lien vers HUGO :  https://gohugo.io
- Lien vers  ghostwriter : https://themes.gohugo.io/ghostwriter/



Sont disponibles pour l'instant :
- Cartographie
- Données Structurées (incomplet)
- Le WEB 
- La photo numérique (incomplet)
- Aide mémoire Python  

Voir le dossier `SOURCES` pour plus de détail
