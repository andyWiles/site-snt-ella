from random import randint
choix=randint(1,100)
essai=1
devine=int(input("J'ai choisi un nombre entier entre 1 et 100. \nEssais de le deviner : "))
while devine!=choix :
    if devine > choix:
        print("Trop grand.")
    else:
        print("Trop petit.")
    essai=essai+1
    devine=int(input("Essais encore : "))
print("Tu as trouvé en", essai,"coups.")
