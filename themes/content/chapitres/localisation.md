---
title: "La géolocalisation"
date: 2019-06-10T17:26:13+02:00
background : "localisation/localisation.png"
---

![localisation](localisation.png#small-align-right)







| Lien vers la séance   | Description  |
|:---------:|:------------:|
| [1](1)          | INTRODUCTION                            | 
| [2](2)         | ANALYSE DES DONNÉES GPS                 | 
| [3](3)          | CRÉATION ET ÉTUDE D’UNE CARTE EN PYTHON |
| [4](4)          | CALCULER DES ITINÉRAIRES                |
| [5](5)          | ANALYSE DES DONNÉES EXIF                |
| [6](6)          | TÂCHE FINALE                            |




 
