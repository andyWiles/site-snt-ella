---
title: "Géolocalisation - séance 3"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "localisation"
---
![localisation](../localisation.png#small-align-right)


##  Création d'une carte en Python

### But de la séance

- Comprendre comment on peut créer une carte interactive en Python
- Comprendre comment on peut coller sur cette carte des repères définis par leurs coordonnées GPS
- Comprendre comment un peut importer un fichier CSV en Python



{{% vignettepdf "télécharger la fiche LOCALISATION3" %}}../CARTO_GEO_S3__OK.pdf{{% /vignettepdf %}}


### Documents et bibliothèques nécessaires

1. Bibliothèques `folium`et `pandas`.
2. Script Python [S3_folium_lycees_isere_v3.py](S3_folium_lycees_isere_v3.py)
3. Fichier de données des établissements de France : [S3_etablissements.csv](S3_etablissements.csv)
