% SNT - 2019
% 
% Fiche 1
%
% (C) Jérémy Chevillot (contenu) - Stéphane Colomban (charte graphique) (CC : by-nc-sa)
% Utilisation  de l'extension snt-ella.sty

\documentclass[a4paper,10pt]{scrartcl}
\usepackage{snt-ella-sans-numero}




\lfoot{ \vspace{-0.5cm }\copyright \quad  J. CHEVILLOT - S. COLOMBAN  - Lycée Ella Fitzgerald - Année 2019/2020 \center \thepage /\pageref{LastPage}}


\begin{document}

\SNT{Objets}{Connectés}{Séance 2 - Programmation évènementielle\\ sur carte micro$:$bit}

\bigskip

\section*{Boutons}

\remarque{red}{!}{A retenir}
\cadre{red}{
Il y a \textbf{deux boutons A et B} sur la face avant de la carte Micro$:$bit.\\
On peut détecter quand ces boutons sont pressés, ce qui permet de déclencher des instructions sur l'appareil.
\begin{itemize}
\item[•] \texttt{button\_a.is\_pressed()}: renvoie \textbf{True} si le bouton A est actuellement enfoncé et \textbf{False} sinon
\item[•] \texttt{button\_a.was\_pressed()}: renvoie \textbf{True ou False} pour indiquer si le bouton A a été appuyé depuis le démarrage de l'appareil ou la dernière fois que cette méthode a été appelée
\item[•] \texttt{button\_a.get\_presses()}: renvoie le nombre de fois où on a appuyé sur le bouton A
\end{itemize}
 }
 
 \medskip
 
Avant de commencer, ouvrir le logiciel \textit{Mu} et connecter votre carte Micro$:$bit sur un port USB.

\medskip

\remarque{vert}{E}{Exercice 1 : Pixel en ballade}

\lstset{language=python}
\begin{lstlisting}[numbers=left,firstnumber=1]
from microbit import *

x = 0
y = 0
while True:
  display.set_pixel(x,y,0)
  if button_a.was_pressed():
    x = x-1
  if button_b.was_pressed():
    x = x + 1
  x = max(0, min(x, 4))
  display.set_pixel(x,y,9)
  sleep(20)
\end{lstlisting}

\begin{enumerate}
\item Flasher ce programme sur la carte puis expliquer ce qu'il fait.
\item Quel est le rôle de la commande \texttt{x = max(0, min(x, 4))} en ligne 11 ?
\item Modifier ce programme pour que le pixel se balade cette fois-ci sur la première colonne.
\end{enumerate}

\medskip

\remarque{vert}{E}{Exercice 2 : Clignotants}

\lstset{language=python}
\begin{lstlisting}[numbers=left,firstnumber=1]
from microbit import *

while True:
  if button_a.is_pressed():
    ...
  if button_b.is_pressed():
    ...
\end{lstlisting}

\begin{enumerate}
\item Compléter ce programme afin qu'il affiche une flèche clignotante vers la gauche lorsque le bouton A est enfoncé et une flèche clignotante vers la droite lorsque le bouton B est enfoncé.
\item Flasher le programme sur la carte et vérifier qu'il fonctionne.
\item Modifier ce programme pour afficher en plus une flèche de recul (vers le bas) lorsque les deux boutons sont enfoncés.\\
\underline{Indication} : On pourra modifier les premières instructions conditionnelles pour indiquer qu'un seul bouton est enfoncé.
\end{enumerate}

\appelprofesseur

\newpage

\section*{Capteurs}
 
 \remarque{blue}{?}{Capteur de lumière}

\cadre{blue}{
En inversant les LEDs d'un écran pour devenir un point d'entrée, l'\textbf{écran LED} devient un capteur de lumière basique, permettant de détecter la luminosité ambiante.\\ \\
La commande \texttt{display.read\_light\_level()} retourne un entier compris entre 0 et 255 représentant le niveau de lumière.
}

\remarque{vert}{E}{Exercice 3 : Le soleil a rendez-vous avec la lune}

\begin{minipage}{\textwidth-3cm}
\lstset{language=python}
\begin{lstlisting}[numbers=left,firstnumber=1]
from microbit import *

soleil = Image("90909:"
               "09990:"
               "99999:"
               "09990:"
               "90909")

lune = Image("00999:"
             "09990:"
             "09900:"
             "09990:"
             "00999")

while True:
    if display.read_light_level()>...:
        display.show(soleil)
    else:
        display.show(...)
    sleep(10)
\end{lstlisting}
\end{minipage}
\begin{minipage}{3cm}
\microbit{9,0,9,0,9,  0,9,9,9,0     ,9,9,9,9,9,   0,9,9,9,0,      9,0,9,0,9} \\

\microbit{0,0,9,9,9    ,0,9,9,9,0,    9,9,9,9,0,     0,9,9,9,0,   0,0,9,9,9} 


\end{minipage}
\begin{enumerate}
\item Compléter le programme ci-dessus pour afficher une image de lune si on baisse la luminosité (en recouvrant la carte avec sa main par exemple) et un soleil sinon.
\item Modifier le programme pour qu'il affiche en défilement le niveau de luminosité ambiant après le symbole obtenu (soleil/lune).
\end{enumerate}

\medskip

 \remarque{blue}{?}{Capteur de température}

\cadre{blue}{
La carte Micro$:$bit n’a pas un capteur de température dédié. Au lieu de cela, la température fournie est en fait la température de la \textbf{puce de silicium du processeur principal}. Comme le processeur chauffe peu en fonctionnement (c'est un processeur ARM à grande efficacité), sa température est une bonne \textbf{approximation} de la température ambiante.\\ \\
L'instruction \texttt{temperature()} renvoie la température de la carte Micro$:$bit en degrés Celsius.
}

\medskip

\remarque{vert}{E}{Exercice 4 : Thermomètre de secours}

\cadre{vert}{
\'Ecrire un programme qui affiche la température en défilement.
}

\bigskip

\appelprofesseur

\newpage

 \remarque{blue}{?}{Capteur de mouvement}

\cadre{blue}{
Un \textbf{accéléromètre} mesure l'accélération de la carte Micro$:$bit, ce composant détecte quand la carte est en mouvement. Il peut aussi détecter d'autres actions (gestes), par exemple quand elle est secouée, inclinée ou qu'elle tombe.\\

Des accéléromètres sont présents dans les smartphones et les manettes de jeux afin de pouvoir afficher l'image dans le bon sens, ou bien de changer de direction dans un jeu.\\

L'accéléromètre mesure le mouvement selon trois axes :
\begin{itemize}
\item[•] X - l’inclinaison de gauche à droite
\item[•] Y - l’inclinaison d’avant en arrière
\item[•] Z - le mouvement haut et bas
\end{itemize}

L'instruction \texttt{accelerometer.get\_x()} permet de détecter un mouvement de gauche à droite en renvoyant un nombre compris entre -1023 et 1023 (0 étant la position "d'équilibre").\\
L'instruction \texttt{accelerometer.is\_gesture('shake')} renvoie \textbf{True} ou \textbf{False} selon si la carte est secouée.
}

\remarque{vert}{E}{Exercice 5 : En haut, en bas, à gauche, à droite}

\lstset{language=python}
\begin{lstlisting}[numbers=left,firstnumber=1]
from microbit import *

while True:
    abscisse = accelerometer.get_x()
    if abscisse > 500:
        display.show(Image.ARROW_E)
    elif abscisse < -500:
        display.show(Image.ARROW_W)
    else:
        display.show("-")
\end{lstlisting}

\begin{enumerate}
\item Flasher ce programme sur la carte. Expliquer ce qu'il fait.
\item Compléter le programme pour qu'il affiche une flèche vers le haut si on incline la carte en avant et une flèche vers le bas si on l'incline en arrière.
\end{enumerate}

\medskip

\remarque{vert}{E}{Exercice 6 : Lancer de dés}

\cadre{vert}{
\'Ecrire un programme qui simule un dé en affichant une face au hasard lorsque la carte Micro$:$bit est secouée.
}

\medskip

\appelprofesseur

\medskip

 \remarque{blue}{?}{Capteur de direction}

\cadre{blue}{
La \textbf{boussole} détecte le champ magnétique de la Terre, nous permettant de savoir quelle direction la carte Micro$:$bit indique.\\

La boussole doit être étalonnée avant de pouvoir être utilisée. Pour cela, on utilise \texttt{compass.calibrate()} qui exécute un petit jeu. Au départ, la carte fait défiler "Tilt to fill screen" puis il faut incliner la carte pour déplacer le point au centre de l’écran autour jusqu'à remplir la totalité de l’écran.\\

La fonction \texttt{compass.heading()} donne le cap de la boussole sous la forme d'un entier compris entre 0 et 360, représentant l'angle en degrés, dans le sens des aiguilles d'une montre, avec le nord égal à 0.
}

\newpage

\remarque{vert}{E}{Exercice 7 : Rose des vents}

\lstset{language=python}
\begin{lstlisting}[numbers=left,firstnumber=1]
from microbit import *

compass.calibrate()

while True:
    if compass.heading() < ... or compass.heading() > ...:
        display.show(Image.ARROW_N)
    else:
        display.show(Image.DIAMOND_SMALL)
\end{lstlisting}

\begin{enumerate}
\item Compléter le programme ci-dessus afin qu'il indique le Nord.
\item Améliorer le programme pour que la carte Micro$:$bit indique "N", "S", "E" et "O" en fonction de l'orientation de la boussole.
\end{enumerate}

\medskip

\appelprofesseur

\medskip

\remarque{blue}{P}{Pour aller plus loin}

\cadre{blue}{
\textbf{Jeu 1 : } \texttt{Magic8Ball.py} 

\begin{itemize}

\item[•]  Copier le programme suivant appelé  \texttt{Magic8Ball.py} sur la carte  Micro$:$bit.
\item[•]  Modifier son code pour fabriquer un dé électronique à six faces .
\end{itemize}
}

 
 



\lstset{language=python}
\begin{lstlisting}[ ]
from microbit import * 
import random
reponses = ["OUI", "NON", "SUREMENT", "PAS SUR"]
while True:
    display.show("8")
    if accelerometer.was_gesture("shake"):
        display.clear()
        sleep(1000) 
        display.scroll(random.choice(reponses),delay=80)
\end{lstlisting}

\bigskip

\cadre{blue}{
\textbf{Jeu 2 : } \texttt{SimpleSlalom.py} 

\begin{itemize}

\item[•]  Copier le programme suivant appelé \texttt{SimpleSlalom.py} sur la carte  Micro$:$bit.
\item[•]  Modifier son code pour fabriquer un dé électronique à six faces .
\end{itemize}
}


 



\lstset{language=python}
\begin{lstlisting}[ ]
from microbit import * 
import random
reponses = ["OUI", "NON", "SUREMENT", "PAS SUR"]
while True:
    display.show("8")
    if accelerometer.was_gesture("shake"):
        display.clear()
        sleep(1000) 
        display.scroll(random.choice(reponses),delay=80)
\end{lstlisting}
\label{LastPage}
\end{document}