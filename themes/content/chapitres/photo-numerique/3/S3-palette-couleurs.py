# SNT seconde
# Thème : Photographie
#
# Codage des pixels d'une image
#
# (C) Stéphane Colomban 2019  (CC : by - nc - sa)




from tkinter import *
from PIL import Image, ImageTk



def creerImage():
    largeur = 400
    hauteur = 300
    image=Image.new('RGB', (largeur,hauteur))
    for x in range(largeur):
        for y in range(hauteur):
            image.putpixel((x,y),(255,0,0))
 
    return image



###############################################
#### Interface graphique (ne pas modifier) ####

    
    
# Création de la fenêtre principale 
root = Tk()
root.title('Photo numérique - Lycée Ella Fitzgerald - SNT 2019')



# Création d'un widget Canvas 


LARGEUR = 500
HAUTEUR = 500
canevas = Canvas(root, width = LARGEUR, height = HAUTEUR, bg ='lightgray')


img = creerImage()
photo=ImageTk.PhotoImage(img)


canevas.create_image(50,50,anchor=NW, image=photo)
canevas.pack(padx =5, pady =5)



# Mise à jour de l'interface
root.mainloop()
