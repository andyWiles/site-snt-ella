---
title: "Le Web - séance 1"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "le-web"
---
![Le-Web](../leweb.png#small-align-right)


## - WEB séance 1 - Introduction au HTML

### But de la séance 

- Comprendre le principe du balisage html.
- Comprendre la structure d'une page html.
- S'approprier les principales balises html.




<center>{{% docpdf "télécharger la fiche HTML1" %}}../WEB_S1__v2.pdf{{% /docpdf %}} </center>




### Aide mémoire 

##### Structure d'une page html
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> montitre </title>
    </head>
    <body>
        ... tout le corps de la page ...
    </body>
</html>
```

##### Principales balises html
```html
...<br />                                       Retour à la ligne
<p>...</p>                                      Création d’un paragraphe
<ol>...</ol>                                    Création d’une liste numérotée
<ul>...</ul>                                    Création d’une liste non numérotée
<li> ...</li>                                   Chaque item d’une liste
<h1> ... </h1>                                  Très gros titre
<h2> ... </h2>                                  Gros titre
<h3> ... </h3>                                  Moyen titre
<img src= "photo.jpg" />                        Affichage de l’image photo.jpg
<!-- écrire un commentaire personnel -->        Commentaire (invisible à l’écran)
<a href="autrepage.html"> Nom du lien </a>      Lien externe vers la page autrepage.html
<div id="monancre"></div>                       Création d’une ancre
<a href="#monancre"> Nom du lien </a>           Lien interne vers l'ancre  monancre
```



{{% notice note %}} 
Faire une fiche cours afin de mémoriser ces balises.
{{% /notice %}}
