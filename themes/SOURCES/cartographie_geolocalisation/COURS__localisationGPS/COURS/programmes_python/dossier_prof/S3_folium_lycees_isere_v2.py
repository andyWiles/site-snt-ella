# SNT seconde
# Thème Cartographie et localisation
#  
# Lycées de l'Isère
# Utilisation d'un fichier .CSV de coordonnées GPS
#
#(C) Stéphane Colomban 2019  (CC : by - nc - sa)


# bibliothèques nécessaires
import folium
import pandas    #pour importer et analyser le .csv

   

#Création d'une carte interactive de l'Isère
carte= folium.Map(location=[45.188529, 5.724524],zoom_start=9)


#Ajout des lycées sur la carte
fichierlycees = pandas.read_csv('export_etab_test.csv',usecols = ["Nom", "Latitude", "Longitude"])
#print (fichierlycees.head(3))
for n in range(len(fichierlycees["Nom"])):
    folium.Marker(location=[fichierlycees["Latitude"][n], fichierlycees["Longitude"][n]], popup=fichierlycees["Nom"][n]).add_to(carte)
    


#sauvegarde le la carte
carte.save('carteISERE.html')
