# SNT seconde
# Thème : Photographie
#
# Création d'une interface graphique pour manipuler des images
# Séance 3 - Créer ses propres filtres photo
#
# (C) Stéphane Colomban 2019  (CC : by - nc - sa)




from tkinter import *
from PIL import Image, ImageTk

def charger_image(filename, resize=None):
    img = Image.open(filename)
 
    if resize is not None:
        img = img.resize(resize, Image.ANTIALIAS)
 
    return img
 
 

def modif_couleurs(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
    for x in range(largeur):
        for y in range(hauteur):
            (rouge,vert,bleu) = img.getpixel((x,y))
            image.putpixel((x,y),(vert,bleu,rouge))
             
 
    return image


def composanteverte(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
    for x in range(largeur):
        for y in range(hauteur):
            (rouge,vert,bleu) = img.getpixel((x,y))
            rouge = 0
            bleu = 0
            
            image.putpixel((x,y),(rouge,vert,bleu))
             
 
    return image


def noiretblanc(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
    for x in range(largeur):
        for y in range(hauteur):
            (rouge,vert,bleu) = img.getpixel((x,y))
            # A compléter
             
 
    return image

def miroirh(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
    for x in range(largeur):
        for y in range(hauteur):
            (rouge,vert,bleu) = img.getpixel((largeur-1-x,y))
            image.putpixel((x,y),(rouge,vert,bleu))
             
 
    return image



def miroirv(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
     # A compléter
             
 
    return image


def eclaicir(img):
    (largeur, hauteur)= img.size
    image=Image.new('RGB', (largeur,hauteur))
     # A compléter
             
 
    return image






img0 = charger_image('photo.png', resize=(200,200) )
img1 = composanteverte(img0)
img2 = miroirh(img0)
img3 = noiretblanc(img0)
img4 = miroirv(img0)
img5 = eclaicir(img0) 



###############################################
#### Interface graphique (ne pas modifier) ####


def motion(event):
    """ Evénements liés à la souris """
    x=event.x
    y=event.y
    
     
def actionBouton1():
    """ Actions du bouton 1"""
    canevas.create_image(350,50,anchor=NW, image=photo1)
    canevas.pack(padx =5, pady =5)

def actionBouton2():
    """ Actions du bouton 2"""
    canevas.create_image(650,50,anchor=NW, image=photo2)
    canevas.pack(padx =5, pady =5)

def actionBouton3():
    """ Actions du bouton 3"""
    canevas.create_image(50,300,anchor=NW, image=photo3)
    canevas.pack(padx =5, pady =5)

def actionBouton4():
    """ Actions du bouton 4"""
    canevas.create_image(350,300,anchor=NW, image=photo4)
    canevas.pack(padx =5, pady =5)
    
def actionBouton5():
    """ Actions du bouton 5"""
    canevas.create_image(650,300,anchor=NW, image=photo5)
    canevas.pack(padx =5, pady =5)
    
    
# Création de la fenêtre principale 
root = Tk()
root.title('Photo numérique séance 3- Lycée Ella Fitzgerald - SNT 2019')



# Création d'un widget Canvas 


LARGEUR = 900
HAUTEUR = 550
canevas = Canvas(root, width = LARGEUR, height = HAUTEUR, bg ='white')

photo0=ImageTk.PhotoImage(img0)
photo1=ImageTk.PhotoImage(img1)
photo2=ImageTk.PhotoImage(img2)
photo3=ImageTk.PhotoImage(img3)
photo4=ImageTk.PhotoImage(img4)
photo5=ImageTk.PhotoImage(img5)

canevas.create_image(50,50,anchor=NW, image=photo0)
canevas.pack(padx =5, pady =5)


cadre_a_boutons = Frame(root, height=200, width=250)
cadre_a_boutons.pack_propagate(0)
cadre_a_boutons.pack()

# Création d'un widget Button (bouton 1)
bouton1 = Button(cadre_a_boutons, text ='composante verte', command = actionBouton1)
bouton1.pack(fill=BOTH,expand=1,pady=5)

# Création d'un widget Button (bouton 2)
bouton2 = Button(cadre_a_boutons, text ='miroir horizontal', command = actionBouton2)
bouton2.pack(fill=BOTH,expand=1,pady=5)

# Création d'un widget Button (bouton 3)
bouton3 = Button(cadre_a_boutons, text ='noir et blanc (à compléter)', command = actionBouton3)
bouton3.pack(fill=BOTH,expand=1,pady=5)


# Création d'un widget Button (bouton 4)
bouton4 = Button(cadre_a_boutons, text ='miroir vertical (à compléter)', command = actionBouton4)
bouton4.pack(fill=BOTH, expand=1,pady=5)


# Création d'un widget Button (bouton 5)
bouton5 = Button(cadre_a_boutons, text ='éclaicir vertical (à compléter)', command = actionBouton5)
bouton5.pack(fill=BOTH,expand=1,pady=5)



# Mise à jour de l'interface
root.bind('<Motion>', motion)
root.mainloop()
