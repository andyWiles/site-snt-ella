---
title: "Géolocalisation - séance 6"
date: 2019-06-11T17:26:13+02:00
masque : "oui"
chapitre : "localisation"
---
![localisation](../localisation.png#small-align-right)


##   Tâche finale

### But de la séance : vérifier les acquis

- Maîtriser le vocabulaire des coordonnées géographiques
- Comprendre le fonctionnement du gps
- Savoir recherche en ligne des informations de géolocalisation d'un lieu.
- Savoir éditer et modifier un programme python.
- Savoir accéder aux données EXIF d'une photographie numérique (avec par exemple GIMP)
- Savoir interpréter ces données EXIF
- Comprendre comment on peut créer une carte interactive en Python
- Comprendre comment on peut coller sur cette carte des repères définis par leurs coordonnées GPS
- Comprendre comment un peut importer un fichier CSV en Python



{{% vignettepdf " télécharger la fiche LOCALISATION6 - Tâche finale" %}}../CARTO_GEO_S6__OK.pdf{{% /vignettepdf %}}


### Documents nécessaires

1. Programme à compléter [S6_folium_gares.py](S6_folium_gares.py)
1. Photo numérique à analyser [photo-tache-finale.jpg](photo-tache-finale.jpg)
