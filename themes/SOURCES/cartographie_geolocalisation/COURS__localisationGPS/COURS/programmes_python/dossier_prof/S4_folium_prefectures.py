# SNT seconde
# Thème Cartographie et localisation
#  
# Préfectures de AURA
# Utilisation d'un fichier .CSV de coordonnées GPS
#
#(C) Stéphane Colomban 2019  (CC : by - nc - sa)


# bibliothèques nécessaires
import folium
import pandas    #pour importer et analyser un fichier  .csv

   

#Création d'une carte interactive de Auvergne - Rhône Alpes
carte = folium.Map(location=[45.2, 4.5],zoom_start=7.2)

#Ajout de la délimitation des départements sur la carte
fichRegion = "departements-auvergne-rhone-alpes.geojson"
folium.GeoJson(fichRegion,name='AURA').add_to(carte)

#Ajout des Préfectures sur la carte
fichPref = pandas.read_csv('S4_AURA_prefectures.csv',delimiter=";")
for n in range(len(fichPref["Prefecture"])):
    pref =fichPref["Prefecture"][n]
    lat =float(fichPref["Latitude"][n])
    lon =float(fichPref["Longitude"][n])
    print(pref)
    folium.CircleMarker(location=[lat, lon], radius=9,color='#ff0000',fill_color='#ff0000',fill=True,popup=pref).add_to(carte)
 
#sauvegarde le la carte
carte.save('cartePREFECTURES.html')
